class CreateStores < ActiveRecord::Migration[5.1]
  def change
    create_table :stores do |t|
      t.references :company, foreign_key: true
      t.string :name
      t.string :code
      t.string :store_type

      t.timestamps
    end
  end
end
