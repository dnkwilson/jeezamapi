class RenameTypeToTransactionTypeOnDrawerTransactions < ActiveRecord::Migration[5.1]
  def change
    rename_column :drawer_transactions, :type, :transaction_type
  end
end
