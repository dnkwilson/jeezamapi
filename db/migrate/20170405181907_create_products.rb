class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.references :category, foreign_key: true
      t.decimal :price_retail
      t.decimal :price_wholesale
      t.decimal :price_bulk
      t.integer :qty_wholesale
      t.integer :qty_bulk

      t.timestamps
    end
  end
end
