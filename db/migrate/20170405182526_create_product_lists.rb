class CreateProductLists < ActiveRecord::Migration[5.1]
  def change
    create_table :product_lists do |t|
      t.string :name
      t.references :store, foreign_key: true

      t.timestamps
    end
  end
end
