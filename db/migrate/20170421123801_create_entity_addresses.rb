class CreateEntityAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :entity_addresses do |t|
      t.string :address
      t.string :address2
      t.string :city
      t.string :state
      t.string :zipcode
      t.string :country
      t.bigint :entity_addressable_id
      t.string :entity_addressable_type

      t.timestamps
    end
  end
end
