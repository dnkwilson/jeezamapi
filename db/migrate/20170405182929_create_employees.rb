class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.references :store, foreign_key: true
      t.references :user,  foreign_key: true
      t.string :start_date
      t.string :end_date
      t.string :role

      t.timestamps
    end
  end
end
