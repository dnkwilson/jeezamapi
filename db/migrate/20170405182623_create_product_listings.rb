class CreateProductListings < ActiveRecord::Migration[5.1]
  def change
    create_table :product_listings do |t|
      t.references :product_list, foreign_key: true
      t.references :product,      foreign_key: true
      t.integer :position,        unique: true

      t.timestamps
    end
  end
end
