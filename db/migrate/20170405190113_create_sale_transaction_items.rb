class CreateSaleTransactionItems < ActiveRecord::Migration[5.1]
  def change
    create_table :sale_transaction_items do |t|
      t.references :sale_transaction, foreign_key: true
      t.references :product, foreign_key: true
      t.string :code
      t.decimal :unit_price
      t.decimal :total_price
      t.decimal :qty

      t.timestamps
    end
  end
end
