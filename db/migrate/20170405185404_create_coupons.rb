class CreateCoupons < ActiveRecord::Migration[5.1]
  def change
    create_table :coupons do |t|
      t.references :company, foreign_key: true
      t.string :name
      t.string :type
      t.integer :rate

      t.timestamps
    end
  end
end
