class CreateDrawerTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :drawer_transactions do |t|
      t.references :drawer, foreign_key: true
      t.references :employee, foreign_key: true
      t.string :type
      t.string :name
      t.string :date
      t.decimal :amount

      t.timestamps
    end
  end
end
