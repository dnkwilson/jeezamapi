class CreateDrawers < ActiveRecord::Migration[5.1]
  def change
    create_table :drawers do |t|
      t.references :store, foreign_key: true
      t.string :name
      t.string :printer

      t.timestamps
    end
  end
end
