class CreateSaleTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :sale_transactions do |t|
      t.references :customer, foreign_key: true
      t.references :drawer, foreign_key: true
      t.references :employee, foreign_key: true
      t.references :coupon, foreign_key: true
      t.decimal :subtotal
      t.decimal :discount
      t.decimal :total
      t.decimal :amount_tendered
      t.decimal :change_issued
      t.string :date
      t.string :payment_type
      t.string :status

      t.timestamps
    end
  end
end
