# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170610182028) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_categories_on_company_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "logo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "logo_data"
  end

  create_table "coupons", force: :cascade do |t|
    t.bigint "company_id"
    t.string "name"
    t.string "type"
    t.integer "rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_coupons_on_company_id"
  end

  create_table "customers", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_customers_on_user_id"
  end

  create_table "drawer_transactions", force: :cascade do |t|
    t.bigint "drawer_id"
    t.bigint "employee_id"
    t.string "transaction_type"
    t.string "name"
    t.string "date"
    t.decimal "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["drawer_id"], name: "index_drawer_transactions_on_drawer_id"
    t.index ["employee_id"], name: "index_drawer_transactions_on_employee_id"
  end

  create_table "drawers", force: :cascade do |t|
    t.bigint "store_id"
    t.string "name"
    t.string "printer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_drawers_on_store_id"
  end

  create_table "employees", force: :cascade do |t|
    t.bigint "store_id"
    t.bigint "user_id"
    t.string "start_date"
    t.string "end_date"
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_employees_on_store_id"
    t.index ["user_id"], name: "index_employees_on_user_id"
  end

  create_table "entity_addresses", force: :cascade do |t|
    t.string "address"
    t.string "address2"
    t.string "city"
    t.string "state"
    t.string "zipcode"
    t.string "country"
    t.bigint "entity_addressable_id"
    t.string "entity_addressable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_listings", force: :cascade do |t|
    t.bigint "product_list_id"
    t.bigint "product_id"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_listings_on_product_id"
    t.index ["product_list_id"], name: "index_product_listings_on_product_list_id"
  end

  create_table "product_lists", force: :cascade do |t|
    t.string "name"
    t.bigint "store_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_product_lists_on_store_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.bigint "category_id"
    t.decimal "price_retail"
    t.decimal "price_wholesale"
    t.decimal "price_bulk"
    t.integer "qty_wholesale"
    t.integer "qty_bulk"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_products_on_category_id"
  end

  create_table "sale_transaction_items", force: :cascade do |t|
    t.bigint "sale_transaction_id"
    t.bigint "product_id"
    t.string "code"
    t.decimal "unit_price"
    t.decimal "total_price"
    t.decimal "qty"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_sale_transaction_items_on_product_id"
    t.index ["sale_transaction_id"], name: "index_sale_transaction_items_on_sale_transaction_id"
  end

  create_table "sale_transactions", force: :cascade do |t|
    t.bigint "customer_id"
    t.bigint "drawer_id"
    t.bigint "employee_id"
    t.bigint "coupon_id"
    t.decimal "subtotal"
    t.decimal "discount"
    t.decimal "total"
    t.decimal "amount_tendered"
    t.decimal "change_issued"
    t.string "date"
    t.string "payment_type"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["coupon_id"], name: "index_sale_transactions_on_coupon_id"
    t.index ["customer_id"], name: "index_sale_transactions_on_customer_id"
    t.index ["drawer_id"], name: "index_sale_transactions_on_drawer_id"
    t.index ["employee_id"], name: "index_sale_transactions_on_employee_id"
  end

  create_table "stores", force: :cascade do |t|
    t.bigint "company_id"
    t.string "name"
    t.string "code"
    t.string "store_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_stores_on_company_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "first_name"
    t.string "last_name"
    t.string "provider"
    t.string "uid"
    t.string "dob"
    t.string "gender"
    t.string "phone"
    t.string "auth_token"
    t.string "username"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "categories", "companies"
  add_foreign_key "coupons", "companies"
  add_foreign_key "customers", "users"
  add_foreign_key "drawer_transactions", "drawers"
  add_foreign_key "drawer_transactions", "employees"
  add_foreign_key "drawers", "stores"
  add_foreign_key "employees", "stores"
  add_foreign_key "employees", "users"
  add_foreign_key "product_listings", "product_lists"
  add_foreign_key "product_listings", "products"
  add_foreign_key "product_lists", "stores"
  add_foreign_key "products", "categories"
  add_foreign_key "sale_transaction_items", "products"
  add_foreign_key "sale_transaction_items", "sale_transactions"
  add_foreign_key "sale_transactions", "coupons"
  add_foreign_key "sale_transactions", "customers"
  add_foreign_key "sale_transactions", "drawers"
  add_foreign_key "sale_transactions", "employees"
  add_foreign_key "stores", "companies"
end
