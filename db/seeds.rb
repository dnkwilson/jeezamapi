# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Company.destroy_all
name = "Coalstove Restaurant & Bar"
params = {company: {name: name, logo: File.new("factory/coalstove_logo.png")},
  store: {name: name, code: "CSNK", store_type: "restaurant"}, address: "17a Dominica Drive",
  city: "New Kingston", state: "Kingston", zipcode: 5, country: "Jamaica",
  email: "tony@coalstove.com", phone: "8849494", username: "tonyblack", password: "password",
  password_confirmation: "password", first_name: "Anthony", last_name: "Wilson", gender: "male"}

user = User.register(params)
store = user.employee.store
company = store.company

Category.destroy_all
breakfast = company.categories.create(name: "Breakfast")
lunch = company.categories.create(name: "Lunch")
misc = company.categories.create(name: "Misc")

Product.destroy_all
curry_chicken = breakfast.products.create!(name: "Curry Chicken", price_small: 350, price_large: 400)
stew_chicken = breakfast.products.create!(name:  "Stew Chicken", price_small: 350, price_large: 400)
ackee_saltfish = breakfast.products.create!(name: "Ackee & Saltfish", price_small: 350, price_large: 400)
okra_saltfish = breakfast.products.create!(name:  "Okra & Saltfish", price_small: 350, price_large: 400)
steam_veg = breakfast.products.create!(name:  "Steamed Vegetables", price_small: 350, price_large: 400)
p "Created #{breakfast.products.count} breakfast products"

fry_chicken = lunch.products.create!(name:  "Fry Chicken", price_small: 350, price_large: 400)
bbf_chicken = lunch.products.create!(name:  "Bar-B-Fry Chicken", price_small: 350, price_large: 400)
bbq_chicken = lunch.products.create!(name:  "Bar-B-Que Chicken", price_small: 350, price_large: 400)
bake_chicken = lunch.products.create!(name:  "Bake Chicken", price_small: 350, price_large: 400)
jerk_chicken = lunch.products.create!(name:  "Jerk Chicken", price_small: 350, price_large: 400)
sweet_sour_chicken = lunch.products.create!(name: "Sweet & Sour Chicken", price_small: 350, price_large: 400)
cow_foot = lunch.products.create!(name: "Cow Foot", price_small: 350, price_large: 400)
stew_peas = lunch.products.create!(name: "Stew Peas", price_small: 350, price_large: 400)
curry_goat = lunch.products.create!(name: "Curry Goat", price_small: 400, price_large: 450)
roast_beef = lunch.products.create!(name: "Roast Beef", price_small: 350, price_large: 400)
stew_beef = lunch.products.create!(name: "Stew Beef", price_small: 350, price_large: 400)
roast_pork = lunch.products.create!(name: "Roast Pork", price_small: 350, price_large: 400)
stew_pork = lunch.products.create!(name: "Stew Pork", price_small: 350, price_large: 400)
jerk_pork = lunch.products.create!(name: "Jerk Pork", price_small: 350, price_large: 400)
p "Created #{lunch.products.count} lunch products"

juice = misc.products.create!(name: "J's Natural Juice")
red_peas_soup = misc.products.create!(name: "Red Peas Soup", price_small: 100, price_large: 170, price_medium: 300)
pumpkin_soup = misc.products.create!(name: "Pumpkin Soup", price_small: 100, price_large: 170, price_medium: 300)
chicken_soup = misc.products.create!(name: "Chicken Soup", price_small: 100, price_large: 170, price_medium: 300)
p "Created #{misc.products.count} misc products"

ProductList.destroy_all
lunch = store.product_lists.create!(name: "Lunch Menu", products: lunch.products)
breakfast = store.product_lists.create!(name: "Breakfast Menu", products: breakfast.products)
juices_plus = store.product_lists.create!(name: "Juice & Soup Menu", products: misc.products)
# todays_menu = store.product_lists.create!(name: "Today's Menu", products: [ackee_saltfish, stew_chicken, bbf_chicken, fry_chicken, stew_beef, curry_goat, jerk_pork, red_peas_soup])
p "Created #{ProductList.count} product lists"
