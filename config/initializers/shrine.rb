require "shrine"
require "shrine/storage/file_system"
require "shrine/storage/s3"

s3_options = {
  access_key_id:     ENV['AWS_ACCESS_KEY_ID'],
  secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
  region:            'us-east-1',
  # region:            ENV['AWS_REGION'],
  bucket:            ENV['FOG_DIRECTORY'],
}

aws_hash = {
  # cache: Shrine::Storage::S3.new(prefix: "cache", **s3_options),
  cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"),
  store: Shrine::Storage::S3.new(prefix: "store", **s3_options),
}

file_hash = {
  cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"), # temporary
  store: Shrine::Storage::FileSystem.new("public", prefix: "uploads/store"), # permanent
}

Shrine.storages = Rails.env.production? ? aws_hash : file_hash

Shrine.plugin :activerecord # or :sequel
Shrine.plugin :cached_attachment_data # for forms
