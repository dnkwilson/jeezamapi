Rails.application.routes.draw do

  devise_for :users,  path: '', path_names: { sign_in: 'login', sign_out: 'logout',
    sign_up: 'register', cancel: 'deauthorize'},
    controllers: {registrations: 'user/registrations', sessions: 'user/sessions'}

    devise_scope :user do
      post "register", to: 'user/registrations#create'
    end

  namespace :admin do
    resources :users
    resources :employees
    resources :stores
    resources :categories
    resources :companies
    resources :products
    resources :product_lists
    resources :product_listings

    root to: "users#index"
  end

  resources :stores do
    resources :sale_transactions, path: "sales"
    resources :drawer_transactions
  end
  resources :categories
  resources :companies
  resources :products
  resources :product_lists

  root to: "users#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
