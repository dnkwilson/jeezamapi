FactoryGirl.define do
  factory :user do
    email                 Faker::Internet.email
    first_name            Faker::Name.first_name
    last_name             Faker::Name.last_name
    gender                ["male", "female"].sample
    dob                   Faker::Date.between(35.years.ago, 13.years.ago)
    phone                 Faker::PhoneNumber.phone_number
    username              Faker::Internet.user_name
    password              "password"
    password_confirmation "password"

    factory :user_with_customer do
      association :customer, factory: :customer
    end

    factory :user_with_employee do
      association :employee, factory: :employee
      association :store
      role        "employee"
      start_date  DateTime.now
      term_date   nil

      trait :as_manager do
        role "manager"
      end
    end

    factory :super_admin_user do
      role "super_admin"
    end
  end
end
