FactoryGirl.define do
  factory :sale_transaction do
    customer nil
    drawer nil
    employee nil
    references "MyString"
    coupon nil
    subtotal "9.99"
    discount "9.99"
    total "9.99"
    amount_tendered "9.99"
    change_issued "9.99"
    date "MyString"
    payment_type "MyString"
    status "MyString"
  end
end
