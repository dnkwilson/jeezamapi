FactoryGirl.define do
  factory :sale_transaction_item do
    sale_transaction nil
    product nil
    code "MyString"
    unit_price "9.99"
    total_price "9.99"
    qty "9.99"
  end
end
