FactoryGirl.define do
  factory :store do
    association :company
    name Faker::Company.name
  end
end
