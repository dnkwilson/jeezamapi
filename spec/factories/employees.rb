FactoryGirl.define do
  factory :employee do
    user ""
    start_date "MyString"
    end_date "MyString"
    store nil
    role "MyString"
  end
end
