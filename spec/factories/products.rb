FactoryGirl.define do
  factory :product do
    name "MyString"
    category nil
    price_retail "9.99"
    price_wholesale "9.99"
    price_bulk "9.99"
    qty_wholesale 1
    qty_bulk 1
  end
end
