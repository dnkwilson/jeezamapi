FactoryGirl.define do
  factory :drawer do
    store nil
    name "MyString"
    printer "MyString"
  end
end
