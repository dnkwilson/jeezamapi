FactoryGirl.define do
  factory :drawer_transaction do
    drawer nil
    employee nil
    type ""
    name "MyString"
    date "MyString"
    amount "9.99"
  end
end
