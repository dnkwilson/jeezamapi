FactoryGirl.define do
  factory :entity_address do
    address "MyString"
    address2 "MyString"
    city "MyString"
    state "MyString"
    zipcode "MyString"
    country "MyString"
    entity_addressable_id ""
    entity_addressable_type "MyString"
  end
end
