FactoryGirl.define do
  factory :product_list_item do
    product_list nil
    product nil
    position 1
  end
end
