module AuthHelper
  def http_login(pw)
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(pw)
  end

  def login_user(user=FactoryGirl.create(:user))
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      http_login(user.auth_token)
      # user.confirm! # or set a confirmed_at inside the factory. Only necessary if you are using the "confirmable" module
      sign_in user
    end
  end
end

RSpec.configure do |config|
  config.include Devise::TestHelpers, :type => :controller
  config.include AuthHelper, :type => :controller
end
