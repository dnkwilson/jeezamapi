require 'rails_helper'

RSpec.describe User::RegistrationsController, type: :controller do

  describe "#new" do
    let(:password) { "password" }
    let(:store_type) { "retail" }
    let(:role) { "employee" }
    let(:gender) { ["male", "female"].sample }
    let(:sign_up_params) {
      {
        company: {name: Faker::Company.name, logo: ""},
        email:      Faker::Internet.email,
        username:   Faker::Internet.user_name,
        phone:      Faker::PhoneNumber.cell_phone,
        first_name: Faker::Name.first_name,
        last_name:  Faker::Name.last_name,
        gender:     gender,
        dob:        Faker::Date.between(35.years.ago, 13.years.ago),
        role:       role,
        address:    Faker::Address.street_address,
        address2:   Faker::Address.secondary_address,
        city:       Faker::Address.city,
        state:      Faker::Address.state,
        zipcode:    Faker::Address.zip,
        country:    Faker::Address.country,
        password: password,
        password_confirmation: password,
        store: {store_type: store_type, code: "ABC123", name: Faker::Company.name}
      }
    }

    before do
      request.env['devise.mapping'] = Devise.mappings[:user]
    end

    it "returns created user" do
      post :create, {params: {user: sign_up_params}, format: :json}
      json = JSON.parse(response.body)

      expect(json["user"]["email"]).to                  eq sign_up_params[:email]
      expect(json["user"]["first_name"]).to             eq sign_up_params[:first_name]
      expect(json["user"]["last_name"]).to              eq sign_up_params[:last_name]
      expect(json["user"]["username"]).to               eq sign_up_params[:username]
      expect(json["user"]["phone"]).to                  eq sign_up_params[:phone]
      expect(json["user"]["dob"]).to                    eq sign_up_params[:dob].to_s
      expect(json["user"]["gender"]).to                 eq sign_up_params[:gender]
      expect(json["user"]["auth_token"]).not_to         be_nil

      expect(json["user"]["employee"]["store"]["company"]["name"]).to eq sign_up_params[:company][:name]
      expect(json["user"]["employee"]["store"]["company"]["logo"]).to eq sign_up_params[:company][:logo]
      expect(json["user"]["employee"]["store"]["name"]).to            eq sign_up_params[:store][:name]
      expect(json["user"]["employee"]["store"]["code"]).to            eq sign_up_params[:store][:code]
      expect(json["user"]["employee"]["store"]["store_type"]).to      eq sign_up_params[:store][:store_type]
      
      expect(json["user"]["employee"]["start_date"]).to eq Date.today.to_s
      expect(json["user"]["employee"]["end_date"]).to   be_nil
      expect(json["user"]["employee"]["role"]).to       eq "admin"
    end
  end
end
