require 'rails_helper'

RSpec.describe User, type: :model do

  describe '#register' do
    let(:role) { "employee" }
    let(:password) { "password" }
    let(:store_type) { ["wholesale", "retail"].sample }
    let(:sign_up_params) {
                            {
                              company: { name: Faker::Company.name, logo: ""},
                              address:      Faker::Address.street_address,
                              address2:     Faker::Address.secondary_address,
                              city:         Faker::Address.city,
                              state:        Faker::Address.state,
                              zipcode:      Faker::Address.zip,
                              country:      Faker::Address.country,
                              email:        Faker::Internet.email,
                              username:     Faker::Internet.user_name,
                              role:         role,
                              password:     password,
                              password_confirmation: password,
                              store: {store_type: store_type, code: "ABC123", name: Faker::Company.name},
                              communication_methods: [{name: "email", address: Faker::Internet.email}]
                            }
                          }
    let(:register) { User.register(sign_up_params) }
    let(:employee) { register.employee }
    let(:store_address) { employee.store.entity_addresses.first }

    it "creates new Company" do
      expect{register}.to change{Company.count}.by(1)
    end

    it "creates new Store" do
      expect{register}.to change{Store.count}.by(1)
    end

    it "creates new Address" do
      expect{register}.to               change{EntityAddress.count}.by(1)
      expect(store_address.address).to  eq sign_up_params[:address]
      expect(store_address.address2).to eq sign_up_params[:address2]
      expect(store_address.city).to     eq sign_up_params[:city]
      expect(store_address.state).to    eq sign_up_params[:state]
      expect(store_address.zipcode).to  eq sign_up_params[:zipcode]
      expect(store_address.country).to  eq sign_up_params[:country]
      expect(store_address.state).to    eq sign_up_params[:state]
    end

    it "creates new User" do
      expect(register.first_name).to            eq sign_up_params[:first_name]
      expect(register.last_name).to             eq sign_up_params[:last_name]
      expect(register.email).to                 eq sign_up_params[:email]
      expect(register.password).to              eq sign_up_params[:password]
      expect(register.password_confirmation).to eq sign_up_params[:password_confirmation]
    end

    it "creates new Employee" do
      expect(employee.store.name).to          eq sign_up_params[:store][:name]
      expect(employee.store.store_type).to    eq sign_up_params[:store][:store_type]
      expect(employee.store.code).to          eq sign_up_params[:store][:code]
      expect(employee.store.company.name).to  eq sign_up_params[:company][:name]
    end
  end
end
