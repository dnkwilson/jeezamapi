class DrawerSerializer < ActiveModel::Serializer
  attributes :id, :store_id, :name, :printer
end
