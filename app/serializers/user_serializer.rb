class UserSerializer < ActiveModel::Serializer
  attributes :id, :auth_token, :dob, :email, :first_name, :gender, :last_name, :phone, :provider,
             :uid, :username

  has_one :customer
  has_one :employee
end
