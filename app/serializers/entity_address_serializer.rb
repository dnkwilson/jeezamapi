class EntityAddressSerializer < ActiveModel::Serializer
  attributes :entity_address_id, :entity_address_type, :address, :address2, :street_address, :city,
             :zipcode, :state, :country
end
