class ProductListingSerializer < ActiveModel::Serializer
  attributes :id, :position, :product

  def product
    for_restaurant = object.product_list.store.is_a_restaurant?
    for_restaurant ? RestaurantProductSerializer.new(object.product) : ProductSerializer.new(object.product)
  end
end
