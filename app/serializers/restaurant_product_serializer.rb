class RestaurantProductSerializer < ActiveModel::Serializer
  attributes  :id,
              :name,
              :category_id,
              :price_small,
              :price_medium,
              :price_large
end
