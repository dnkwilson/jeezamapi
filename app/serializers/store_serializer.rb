class StoreSerializer < ActiveModel::Serializer
  attributes :id, :address, :address2, :city, :code, :country, :name,
             :state, :store_type, :zipcode

  belongs_to :company
  has_many :product_lists
  has_many :drawers
  has_many :employees, serializer: PublicEmployeeSerializer
end
