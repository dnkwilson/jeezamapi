class CompanySerializer < ActiveModel::Serializer
  attributes :id, :logo, :name

  def logo
    object.logo_url(:large)
  end
end
