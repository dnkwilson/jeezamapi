class EmployeeSerializer < ActiveModel::Serializer
  attributes :id, :end_date, :role, :start_date, :user_id, :store_id

  # has_many :drawer_transactions
  # has_many :sale_transactions

  # def store
  #   StoreSerializer.new(object.store)
  # end
end
