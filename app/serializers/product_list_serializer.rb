class ProductListSerializer < ActiveModel::Serializer
  attributes :id, :name

  has_many :product_listings

end
