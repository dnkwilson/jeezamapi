class PublicEmployeeSerializer < ActiveModel::Serializer
  attributes :id, :end_date, :first_name, :last_name, :role, :start_date, :user_id

  def first_name
    object.user.first_name
  end

  def last_name
    object.user.last_name
  end
end
