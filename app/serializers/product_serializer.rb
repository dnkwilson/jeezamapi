class ProductSerializer < ActiveModel::Serializer
  attributes  :id,
              :name,
              :category_id,
              :price_small,
              :price_medium,
              :price_large,
              :price_retail,
              :price_wholesale,
              :price_bulk,
              :qty_wholesale,
              :qty_bulk
end
