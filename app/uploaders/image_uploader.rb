require "image_processing/mini_magick"

class ImageUploader < Shrine
  include ImageProcessing::MiniMagick
  plugin :processing
  plugin :versions   # enable Shrine to handle a hash of files
  # plugin :delete_raw # delete processed files after uploading

  process(:store) do |io, context|
    original = io.download

    size_800 = resize_to_limit!(original, 800, 800) { |cmd| cmd.auto_orient } # orient rotated images
    size_500 = resize_to_fill(original,  500, 500)  { |cmd| cmd.auto_orient }
    size_100 = resize_to_fill(size_500,  100, 100)

    {large: size_800, medium: size_500, small: size_100}
  end
end
