class Store < ApplicationRecord
  include EntityAddressable

  belongs_to :company
  has_many :entity_addresses, as: :entity_addressable, dependent: :destroy
  has_many :employees, dependent: :destroy
  has_many :drawers,   dependent: :destroy
  has_many :product_lists, dependent: :destroy

  STORE_TYPE = %w[wholesaler retailer restaurant]

  def is_a_restaurant?
    store_type.eql?('restaurant')
  end
end
