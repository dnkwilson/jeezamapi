class Product < ApplicationRecord
  belongs_to :category

  has_many :product_listings, dependent: :destroy
  has_many :product_lists, through: :product_listings

  accepts_nested_attributes_for :product_lists,
                                 reject_if: :all_blank,
                                 allow_destroy: true
  accepts_nested_attributes_for :product_listings

  # Alias column names
  alias_attribute :price_small, :price_retail
  alias_attribute :price_medium, :price_bulk
  alias_attribute :price_large, :price_wholesale
end
