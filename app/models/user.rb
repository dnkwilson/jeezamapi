class User < ApplicationRecord

  before_save :ensure_auth_token, :ensure_username

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :customer, dependent: :destroy
  has_one :employee, dependent: :destroy

  # To enable login with either :username or :email
 	def self.find_for_database_authentication(warden_conditions)
 		conditions = warden_conditions.dup
 		if login = conditions.delete(:login)
      where(conditions.to_hash).find_by_login(login)
 		elsif conditions.has_key?(:username) || conditions.has_key?(:email) || conditions.has_key?(:phone)
 			where(conditions.to_hash).first
 		end
 	end

 	def self.find_by_login(login)
 		includes(employee: [store: [:entity_addresses, product_lists: [:product_listings]]])
    .where(["lower(email) = :value OR lower(phone) = :value OR lower(username) = :value",
 			{ value: login.downcase }]).first
 	end

  def self.register(params)
    ActiveRecord::Base.transaction do
      # Create company
      company   = Company.where(name: params[:company][:name]).first_or_create
      company.update_attribute(:logo, params[:company][:logo])

      # Find or create store
      store     = company.stores.create!(name: params[:store][:name],
                                       code: params[:store][:code],
                                       store_type: params[:store][:store_type])

      # Add address
      store.entity_addresses.create!(address: params[:address],
                                    address2: params[:address2],
                                    city: params[:city],
                                    state: params[:state],
                                    zipcode: params[:zipcode],
                                    country: params[:country])

      store.drawers.create!(name: SecureRandom.uuid.gsub(/\-/,''))

      # Create user
      user = User.create!(email: params[:email],
                          phone: params[:phone],
                          username: params[:username],
                          password: params[:password],
                          password_confirmation: params[:password_confirmation],
                          first_name: params[:first_name],
                          last_name: params[:last_name],
                          gender: params[:gender],
                          dob: params[:dob])

      # Create user type
      if params[:role].eql?("customer")
        user.create_customer(params[:customer])
      else
        user.create_employee(store: store, role: "admin", start_date: Date.today)
      end

      user
    end
  end

  def existing_username?
    where(username: username).first
  end

  def is_admin?
    role.eql?("admin")
  end

  private

  def generate_auth_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(auth_token: token).first
    end
  end

  def ensure_auth_token
    update_attribute(:auth_token, generate_auth_token) if auth_token.blank?
  end

  def generate_username
    loop do |n|
      username = self.email[/^[^@]+/]
      username = "#{username}_#{n}" if existing_username?
      break username unless existing_username?
    end
  end

  def ensure_username
    return unless username.empty?
    update_attribute(:username, generate_username)
  end
end
