class SaleTransaction < ApplicationRecord
  belongs_to :customer
  belongs_to :drawer
  belongs_to :employee
  # belongs_to :coupon

  has_many :sale_transaction_items, dependent: :destroy

  def self.build_sale(params, user)
    return unless user.employee
    # TODO: Fix this
    # customer = Customer.find(params[:customer_id]) || Customer.includes(:user).where(user: {username: "walkin_customer"})
    # drawer = Drawer.find(params[:drawer_id]) || user.employee.store.drawers.first
    customer = Customer.first
    drawer = Drawer.first
    sale = user.employee.sale_transactions.create!(
            customer: customer,
            drawer: drawer,
            subtotal: params[:subtotal],
            discount: params[:discount_total],
            total: params[:sale_total],
            amount_tendered: params[:tendered],
            change_issued: params[:change],
            date: params[:sale_date],
            payment_type: params[:payment_type],
            status: params[:sale_status]
          )
    sale.build_sale_transaction_items(params[:sale_items])
    sale
  end

  def build_sale_transaction_items(params)
    params.each do |sale_item|
      item = sale_transaction_items.create!(
        product_id: sale_item[:product_id],
        code: sale_item[:sale_code],
        unit_price: sale_item[:unit_price],
        total_price: sale_item[:total_price],
        qty: sale_item[:qty]
      )
    end
  end
end
