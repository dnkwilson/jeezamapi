class Company < ApplicationRecord
  include ImageUploader[:logo]  

  has_many :stores, dependent: :destroy
  has_many :categories, dependent: :destroy
end
