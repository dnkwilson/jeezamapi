class ProductList < ApplicationRecord
  belongs_to :store

  has_many :product_listings, dependent: :destroy
  has_many :products, through: :product_listings

  accepts_nested_attributes_for :products,
                                 reject_if: :all_blank,
                                 allow_destroy: true
  accepts_nested_attributes_for :product_listings
end
