class DrawerTransaction < ApplicationRecord
  belongs_to :drawer
  belongs_to :employee
end
