module EntityAddressable
  extend ActiveSupport::Concern

  def entity_address
    entity_addresses.try(:first)
  end

  def address
    entity_address.try(:address)
  end

  def address2
    entity_address.try(:address2)
  end

  def city
    entity_address.try(:city)
  end

  def state
    entity_address.try(:state)
  end

  def zipcode
    entity_address.try(:zipcode)
  end

  def country
    entity_address.try(:country)
  end
end
