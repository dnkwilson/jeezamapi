class Employee < ApplicationRecord
  belongs_to :store
  belongs_to :user, dependent: :destroy
  has_many :drawer_transactions
  has_many :sale_transactions
end
