class EntityAddress < ApplicationRecord
  belongs_to :entity_addressable, polymorphic: true
end
