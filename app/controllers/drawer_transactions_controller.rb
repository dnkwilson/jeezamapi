class DrawerTransactionsController < ApplicationController
  # include Pundit
  before_action :check_authentication
  before_action :set_drawer_transaction, only: [:show, :update]

  respond_to :json

  def create
    # render_unauthorized unless current_user.employee
    @drawer_transaction = current_user.employee.drawer_transactions.new(drawer_transaction_params)
    # authorize @drawer_transaction
    respond_to do |format|
      if @drawer_transaction.save
        format.json {render json: @drawer_transaction, status: :ok}
      else
        format.json {render json: @drawer_transaction.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
  end

  def show
    authorize current_user, :show?
    respond_to do |format|
      format.json {render json: @drawer_transaction, status: :ok}
    end
  end

  def index
    # authorize current_user, :index?
    @drawer_transactions = policy_scope(DrawerTransaction)
    respond_to do |format|
      format.json {render json: @drawer_transaction, status: :ok}
    end
  end

  private

  def set_drawer_transaction
    @drawer_transaction = DrawerTransaction.find(params[:id])
  end

  def drawer_transaction_params
    params.require(:drawer_transaction).permit(:employee_id, :name, :transaction_type, :date,
      :amount, :drawer_id)
  end

  # def drawer_transaction_params
  #   params.require(:drawer_transaction).permit(:id, :employee_id, :store_id, :name,
  #     :type, :date, :amount, :drawer_id)
  # end
end
