class StoresController < ApplicationController
  before_action :set_store, only: [:show, :update]

  respond_to :json

  def index
    @stores = Store.all.page(params[:page])
    respond_to do |format|
      format.json {render json: @stores, meta: meta_params, status: :ok}
    end
  end

  def show
    respond_to do |format|
      format.json {render json: @store, status: :ok}
    end
  end

  def update
  end

  private

  def set_store
    @store = Store.includes(:entity_addresses, :company, :drawers, :employees,
    product_lists: [product_listings: [:product]]).find(params[:id])
  end

  def store_params
    params.require(:stores).permit(:id, :employee_id, :company_id, :name)
  end
end
