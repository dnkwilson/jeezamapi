class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  protect_from_forgery prepend: true

  def check_authentication
    authenticate_with_http_token do |token|
      @current_user = User.where(auth_token: token).first
      render_unauthorized unless @current_user
    end
  end

end
