class SaleTransactionsController < ApplicationController
  before_action :check_authentication
  before_action :set_sale, only: [:show, :update]

  respond_to :json

  def create
    @sale_transaction = SaleTransaction.build_sale(sale_params, current_user)
    # authorize @sale_transaction
    respond_to do |format|
      if @sale_transaction
        format.json {render json: @sale_transaction, status: :ok}
      else
        format.json {render json: @sale_transaction.errors, status: :ok}
      end
    end
  end

  def update
  end

  def show
    respond_to do |format|
      format.json {render json: @sale_transaction, status: :ok}
    end
  end

  def index
    @sale_transactions = SaleTransaction.all.page(params[:page])
    respond_to do |format|
      format.json {render json: @sale_transactions, meta: meta_params, status: :ok}
    end
  end

  private

  def set_sale_transaction
    @sale_transaction = SaleTransaction.includes(:sale_items).find(params[:id])
  end

  def sale_params
    params.require(:sales).permit(:user_id, :customer_id, :store_id, :drawer_id, :subtotal,
                                  :discount_rate, :discount_total, :sale_total, :tendered,
                                  :sale_date, :payment_type, :sale_status, :change,
                                  sale_items: [:sale_id, :product_id, :sale_code, :unit_price,
                                    :total_price, :qty])
  end
end
