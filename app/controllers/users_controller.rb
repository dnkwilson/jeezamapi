class UsersController < ApplicationController
  include Devise::Controllers::SignInOut

  before_action :check_authentication, only: [:logout, :add_employee]

  def login
    user = User.find_for_database_authentication(login: login_params[:login])
		return render_login_failure unless user

		if user.valid_password?(login_params[:password])
			sign_in("user", user)
			render  json: current_user, status: 200
		else
			render_login_failure
		end
  end

  def logout
    current_user.update_column(authentication_token: nil)
    sign_out current_user
		render json: { success: true, info: "Logged out" }, status: 200
  end

  def add_employee
    user = User.add_employee(current_user, egister_params)
    if user
      sign_in("user", user)
      render_login_success
    else
      render_login_failure
    end
  end

  private

  def login_params
    params.require(:session).permit(:login, :password)
  end

  def register_params
    params.require(:users).permit(:dob, :email, :first_name, :last_name, :gender, :username,
                                  :password, :password_confirmation, :phone, :role)
  end
end
