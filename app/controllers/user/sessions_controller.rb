class User::SessionsController < Devise::SessionsController
  include AuthenticatingController
  # skip_before_action :check_authentication, only: :create
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  def create
    user = User.find_for_database_authentication(login: session_params[:login])
    return render_login_failure unless user

    if user.valid_password?(session_params[:password])
      sign_in("user", user)
      render  json: current_user, status: 200
    else
      render_login_failure
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end

  def session_params
    params.require(:session).permit(:login, :password)
  end
end
