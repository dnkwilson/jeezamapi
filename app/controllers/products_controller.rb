class ProductsController < ApplicationController
  # include Pundit

  before_action :set_product, only: [:show, :update]

  respond_to :json

  def create
  end

  def update
  end

  def show
    # authorize current_user, :show?
    respond_to do |format|
      format.json {render json: @product, status: :ok}
    end
  end

  def index
    authorize current_user, :index?
    @products = policy_scope(Product)
    respond_to do |format|
      format.json {render json: @products, status: :ok}
    end
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:id, :store_id, :name, :category_id, :retail_price,
      :wholesale_price, :bulk_price, :item_code, :bulk_qty)
  end
end
