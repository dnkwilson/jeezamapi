module AuthenticatingController
  extend ActiveSupport::Concern

  included do

    def render_login_success
      render  json: current_user, status: 200, meta: {title: "Registation Completed!"}
    end

    def render_logout
      render  json: { meta: {title: "Log Out", message: "Logged out. See you soon!"}}
    end

    def render_unauthorized
      self.headers['HTTP_CONTENT_TYPE'] = 'Token realm="Application"'
      self.headers['Content-Type'] = 'application/json'
      render json: {error: {title: "Unauthorized Access", message: "Unauthorized
        access. You must be logged in to perform this action."}}, status: 401
    end

    def render_login_failure
      render json: {error: {title: "Login Failure", message: "Invalid email or password. Please try
        again."}, status: 200 }
    end
  end
end
