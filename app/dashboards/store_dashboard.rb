require "administrate/base_dashboard"

class StoreDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    company: Field::BelongsTo,
    entity_addresses: Field::HasMany,
    employees: Field::HasMany,
    drawers: Field::HasMany,
    id: Field::Number,
    name: Field::String,
    code: Field::String,
    store_type: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :company,
    :entity_addresses,
    :employees,
    :drawers,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :company,
    :entity_addresses,
    :employees,
    :drawers,
    :id,
    :name,
    :code,
    :store_type,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :company,
    :entity_addresses,
    :employees,
    :drawers,
    :name,
    :code,
    :store_type,
  ].freeze

  # Overwrite this method to customize how stores are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(store)
    # "Store ##{store.id}"
    store.name
  end
end
